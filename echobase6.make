; ADrupalSite Install Profile: provides base install for EchoDitto sites

; Conventions: contrib is used for all projects hosted on drupal.org, vendor/vendor_name for any projects hosted on 3rd party sites

core = 6.x
api = 2

projects[drupal][type] = "core"
projects[drupal][download][type]="git"
projects[drupal][download][revision] = "6.x"
;projects[pressflow][type] = "core"
;projects[pressflow][download][type] = "git"
;projects[pressflow][download][url] = "git://github.com/bigmack83/pressflow-6.git"

; Sitebuilding Modules
projects[ctools][subdir] = "contrib"
projects[install_profile_api][subdir] = "contrib"
;projects[admin][subdir] = "contrib"
projects[simplemenu][subdir] = "contrib"
projects[adminrole][subdir] = "contrib"
projects[advanced_help][subdir] = "contrib"
projects[blockreference][subdir] = "contrib"
projects[better_formats][subdir] = "contrib"
projects[better_perms][subdir] = "contrib"
projects[betterselect][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[filter_perms][subdir] = "contrib"
projects[formdefaults][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"
projects[google_analytics][subdir] = "contrib"
projects[logintoboggan][subdir] = "contrib"
projects[menu_block][subdir] = "contrib"
projects[menutrails][subdir] = "contrib"
projects[nodequeue][subdir] = "contrib"
projects[path_redirect][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[permission_select][subdir] = "contrib"
projects[purl][subdir] = "contrib"
projects[rules][subdir] = "contrib"
projects[suggestedterms][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[panels][subdir] = "contrib"
projects[spaces][subdir] = "contrib"
projects[themesettingsapi][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[workflow][subdir] = "contrib"
projects[customerror][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"

; Development Modules
projects[deploy][subdir] = "contrib"
projects[coder][subdir] = "contrib"
projects[devel][subdir] = "contrib"
projects[devel_themer][subdir] = "contrib"
projects[schema][subdir] = "contrib"
projects[strongarm][subdir] = "contrib"
projects[node_export][subdir] = "contrib"
projects[sitedoc][subdir] = "contrib"
projects[simpletest][subdir] = "contrib"
; commented out due to versioning issues with the project server
;projects[tw] = "contrib"
;projects[sites][version] = "6.x-1.x-dev"
;projects[migrate] = "contrib"
;projects[sites][version] = "6.x-1.x-dev"

; Security
projects[captcha][subdir] = "contrib"
projects[recaptcha][subdir] = "contrib"
projects[login_security][subdir] = contrib
projects[password_policy][subdir] = contrib
projects[salt][subdir] = contrib
projects[phpids][subdir] = contrib
projects[httpbl][subdir] = contrib
projects[persistent_login][subdir] = contrib
projects[single_login][subdir] = contrib
projects[securelogin][subdir] = contrib
projects[badbehavior][subdir] = contrib

; SEO
projects[nodewords][subdir] = "contrib"
projects[page_title][subdir] = "contrib"
projects[xmlsitemap][subdir] = "contrib"

; Social Media Integration
projects[twitter][subdir] = "contrib"

; EchoDitto Modules
projects[partners][subdir] = "contrib"
; commenting out sites module for the time being as it is triggering an
; "invalid version" error when running the makefile
;projects[sites][subdir] = "contrib"
;projects[sites][version] = "6.x-1.x-dev"

; Views Modules
projects[views][subdir] = "contrib"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_customfield][subdir] = "contrib"
projects[jcarousel][subdir] = "contrib"
projects[views_rss][subdir] = "contrib"
projects[views_bonus][subdir] = "contrib"
projects[views_customfield][subdir] = "contrib"
projects[views_slideshow][subdir] = "contrib"
projects[calendar][subdir] = "contrib"

; Content/CCK
projects[cck][subdir] = "contrib"
projects[cck_blocks][subdir] = "contrib"
projects[cck_redirection][subdir] = "contrib"
projects[computed_field][subdir] = "contrib"
projects[content_profile][subdir] = "contrib"
projects[content_type_selector][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[email][subdir] = "contrib"
projects[emfield][subdir] = "contrib"
projects[extlink][subdir] = "contrib"
projects[filefield][subdir] = "contrib"
projects[link][subdir] = "contrib"
projects[webform][subdir] = "contrib"
projects[htmLawed][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[votingapi][subdir] = "contrib"
projects[fivestar][subdir] = "contrib"
projects[filefield_sources][subdir] = "contrib"
projects[viewfield][subdir] = "contrib"
; projects[views_arg_context][subdir] = "contrib"
projects[views_customfield][subdir] = "contrib"

; Admin
projects[addanother][subdir] = "contrib"
projects[nodeformsettings][subdir] = "contrib"
projects[override_node_options][subdir] = "contrib"
projects[total_control][subdir] = "contrib"
projects[masquerade][subdir] = "contrib"

; Image
projects[imce][subdir] = "contrib"
projects[imageapi][subdir] = "contrib"
projects[imagecache][subdir] = "contrib"
projects[imagefield][subdir] = "contrib"
projects[imagefield_extended][subdir] = "contrib"

; Feeds
; projects[feedapi][subdir] = "contrib"
; projects[feedapi_eparser][subdir] = "contrib"
; projects[feedapi_mapper][subdir] = "contrib" feedapi deprecated in favor of feeds module
projects[feeds][subdir] = "contrib"

; Site Visitor Functionality
projects[addthis][subdir] = "contrib"
projects[addtoany][subdir] = "contrib"
projects[forward][subdir] = "contrib"
projects[messaging][subdir] = "contrib"
projects[notifications][subdir] = "contrib"
projects[print][subdir] = "contrib"
projects[service_links][subdir] = "contrib"
projects[textsize][subdir] = "contrib"
projects[faceted_search][subdir] = "contrib"

; WYSIWYG and Editing
projects[fckeditor][subdir] = "contrib"
projects[wysiwyg][subdir] = "contrib"
projects[imce_wysiwyg][subdir] = "contrib"
projects[typogrify][subdir] = "contrib"

; Location/Mapping
projects[gmap][subdir] = "contrib"
projects[location][subdir] = "contrib"

; Javascript Modules
projects[jcarousel][subdir] = "contrib"
projects[jq][subdir] = "contrib"
projects[jquery_plugin][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[jsalter][subdir] = "contrib"
projects[jquery_ui][subdir] = "contrib"

; EchoDitto Modules
projects[inspector][type] = module
projects[inspector][download][type] = "svn"
projects[inspector][download][url] = "https://secure.echoditto.com/repos/echoditto-software/echoditto_inspector/trunk/"
projects[inspector][subdir] = "contrib"
projects[echoditto_conf_toggle][type] = module
projects[echoditto_conf_toggle][download][type] = "svn"
projects[echoditto_conf_toggle][download][url] = "https://secure.echoditto.com/repos/echoditto-software/echoditto_conf_toggle/trunk/"
projects[echoditto_conf_toggle][subdir] = "contrib"

; Patched.
; Explicit versions specified to ensure patches apply cleanly.

; Custom modules
; projects[extractor][subdir] = "custom"
; projects[extractor][location] = "http://code.developmentseed.org/fserver"

; Features
; projects[mn_about][subdir] = "features"
; projects[mn_about][location] = "http://code.developmentseed.org/fserver"

; Starter Themes
projects[blueprint][subdir] = "contrib"
projects[zen][subdir] = "contrib"
projects[omega][subdir] = "contrib"
projects[genesis][subdir] = "contrib"
projects[fusion][subdir] = "contrib"

; Full Themes
projects[a3_atlantis][subdir] = "contrib"
projects[abarre][subdir] = "contrib"
projects[ablock][subdir] = "contrib"
projects[acquia_marina][subdir] = "contrib"
projects[colourise][subdir] = "contrib"
projects[coolwater][subdir] = "contrib"
projects[fervens][subdir] = "contrib"
projects[hiroshige][subdir] = "contrib"
projects[inove][subdir] = "contrib"
projects[marinelli][subdir] = "contrib"
projects[newsflash][subdir] = "contrib"
projects[nitobe][subdir] = "contrib"
projects[painted][subdir] = "contrib"
projects[pixture][subdir] = "contrib"
projects[simply_modern][subdir] = "contrib"
projects[superclean][subdir] = "contrib"
projects[toasted][subdir] = "contrib"
projects[twilight][subdir] = "contrib"
projects[wabi][subdir] = "contrib"

; Admin Themes
projects[rootcandy][subdir] = "contrib"

; vendor themes
projects[tao][subdir] = "vendor/devseed"
projects[tao][location] = "http://code.developmentseed.org/fserver"
projects[rubik][subdir] = "vendor/devseed"
projects[rubik][location] = "http://code.developmentseed.org/fserver"

; Libraries
libraries[simplepie][download][type] = "get"
libraries[simplepie][download][url] = "http://simplepie.org/downloads/simplepie_1.2.zip"
libraries[simplepie][directory_name] = "simplepie"
libraries[ckeditorlib][download][type] = "get"
libraries[ckeditorlib][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.3/ckeditor_3.3.tar.gz"
libraries[ckeditorlib][directory_name] = "ckeditor"

