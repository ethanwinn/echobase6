#!/bin/sh
# This script is responsible for making the various directory structure required for EchoDitto sites
# it will then add all new files to the repo and commit

# TODO: check that we're in the right directory

# Make directories
mkdir -p sites/all/files
mkdir -p sites/all/{modules,themes}/{contrib,custom,vendor}
mkdir -p sites/all/{modules,themes}/vendor/echoditto

# Copy settings.php
cp sites/default/default.settings.php sites/default/settings.php

# Add everything to the repo
svn add .

# set svn:ignore on the files directory
svn propset svn:ignore "*" sites/all/files

# commit
svn ci
